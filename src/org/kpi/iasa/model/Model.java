package org.kpi.iasa.model;

import java.util.Map;

/**
 * Created by student on 02.05.2020.
 */
public class Model {
    private Map<String, String> userData;

    public void saveData(Map<String, String> data) {
        this.userData = data;
    }

    public Map<String, String> getData() {
        return this.userData;
    }
    public String getData(String key) {
        return this.userData.get(key);
    }
}
