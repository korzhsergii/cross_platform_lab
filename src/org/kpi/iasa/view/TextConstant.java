package org.kpi.iasa.view;

/**
 * Created by student on 02.05.2020.
 */
public interface TextConstant {
    String INPUT_STRING_DATA = "input.string.data";
    String OUTPUT_LABEL = "output.label";

    String FIRST_NAME = "input.first.name.data";
    String FIRST_NAME_OUT = "output.first.name";

    String WRONG_INPUT_DATA = "input.wrong.data";

    String LOGIN_DATA = "input.login.data";
    String LOGIN_DATA_OUT = "output.login";

    String MESSAGE_DATA = "input.message.data";
    String MESSAGE_DATA_OUT = "output.message";
}
