package org.kpi.iasa.controller;

import org.kpi.iasa.view.View;
import org.kpi.iasa.view.TextConstant;

import java.util.*;

/**
 * Created by student on 02.05.2020.
 */
public class InputInformationService {
    private View view;
    private Scanner sc;

    private String firstName;
    private String login;
    private String message;

    public InputInformationService(View view, Scanner sc) {
        this.view = view;
        this.sc = sc;
    }

    public void inputNote() {
        UtilityController utilityController =
                new UtilityController(sc, view);
        String str = (String.valueOf(View.bundle.getLocale()).equals("ua"))
                ? RegExContainer.REGEX_NAME_UKR : RegExContainer.REGEX_NAME_LAT;

        this.firstName =
                utilityController.inputStringValueWithScanner
                        (TextConstant.FIRST_NAME, str);

        this.login =
                utilityController.inputStringValueWithScanner
                        (TextConstant.LOGIN_DATA, RegExContainer.REGEX_LOGIN);

        this.message = utilityController.inputStringValueWithScanner
                (TextConstant.MESSAGE_DATA, RegExContainer.REGEX_MESSAGE);
    }

    public Map<String, String> getValues() {
        Map<String, String> valuesList = new HashMap<>();
        valuesList.put(TextConstant.FIRST_NAME_OUT, this.firstName);
        valuesList.put(TextConstant.LOGIN_DATA_OUT, this.login);
        valuesList.put(TextConstant.MESSAGE_DATA_OUT, this.message);

        return valuesList;
    }
}
