package org.kpi.iasa;

import org.kpi.iasa.controller.Controller;
import org.kpi.iasa.model.Model;
import org.kpi.iasa.view.View;

public class Main {

    public static void main(String[] args) {
	    Controller controller =
                new Controller(new Model(), new View());
        controller.processUser();

        controller.showUserData();
    }
}
